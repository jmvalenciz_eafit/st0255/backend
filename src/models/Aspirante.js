var db = require('../database/db');
var mailer = require('./mailer');

var Estudiante = class{
    constructor(){}
    new(nombres, apellidos, zona, carrera, callback){
        db.insertAspirante(nombres, apellidos, zona, carrera)
          .then(status=>{
              console.log(status);
              callback(status);
          })
          .catch(err=>{
              console.log(err);
              callback(err);
          });
    }
    send(email){
        db.getAspirantes_for_mail()
          .then(rows =>{
            mailer.send(email, rows);
          })
          .catch(err =>{
              console.log(err);
          });
    }
}
module.exports = Estudiante
