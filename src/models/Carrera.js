var db = require('../database/db');

Carrera = class{
    constructor(){}
    get_all(callback){
        db.select_from_table("carreras")
          .then(rows =>{
              callback(rows);
          })
          .catch(err =>{
              callback(err);
          });
    }
}
module.exports = Carrera
