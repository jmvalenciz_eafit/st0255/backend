var Router = require('express').Router();
var Zona = require('../models/Zona');

var zona = new Zona();

Router.get('/', (req, res)=>{
    zona.get_all((rows)=>{
        res.send(rows);
    });
});

module.exports = Router
