var Router = require('express').Router();
var Carrera = require('../models/Carrera');

var carrera = new Carrera();

Router.get('/', (req, res)=>{
    carrera.get_all((rows)=>{
        res.send(rows);
    });
});

module.exports = Router
