var Router = require('express').Router();
var Aspirante = require('../models/Aspirante');

var aspirante = new Aspirante();

Router.post('/new', (req, res)=>{
    aspirante.new(req.body.nombres, req.body.apellidos, req.body.zona, req.body.carrera, ()=>{
        res.send({status:0});
    });
});

Router.post('/send/list', function(req, res){
    aspirante.send(req.body.email);
});

module.exports = Router
